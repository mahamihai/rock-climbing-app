﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Rockclimbing.BLL;
using Rockclimbing.Controllers.Mappers;
using Rockclimbing.Controllers.APIModel;
using Rockclimbing.Models;
using Rockclimbing.BLL.Service;

namespace Rockclimbing.Controllers
{
    public class ArealController : ApiController
    {
        private ArealService areal = new ArealService();
        ArealMapper mapper = new ArealMapper();

        [HttpGet]
        [Route("Areals/")]
        [Authorize(Roles = "Admin, Client")]
        public List<ArealModel> Get()
        {
            List<ArealModel> areals = new List<ArealModel>();
            areals = this.areal.getAreals();
            return areals;
        }

        [HttpGet]
        [Route("Areals/{id}")]
        [Authorize(Roles = "Admin, Client")]
        public List<ArealModel> GetById(int id)
        {
            List<ArealModel> areals = new List<ArealModel>();

            areals = this.areal.getAreals();
            var filtered = areals.Where(x => x.Id.Equals(id)).ToList();

            return filtered;
        }

        // POST: api/Areals
        [HttpPost]
        [Route("Areals/")]
        [Authorize(Roles = "Admin")]
        public void Post([FromBody]ArealAPI assi)
        {
            var converted = mapper.AreMapper(assi);
            this.areal.Add(converted);
        }

        [HttpPut]
        [Route("Areals/")]
        [Authorize(Roles = "Admin")]
        // PUT: api/Areals/5
        public void Put([FromBody]ArealModel assi)
        {
            this.areal.Update(assi);
        }
        [HttpDelete]
        [Route("Areals/{id}")]
        [Authorize(Roles = "Admin")]
        // DELETE: api/Areals/5
        public void Delete(int id)
        {
            this.areal.Delete(id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Newtonsoft.Json;
using Rockclimbing.Models;
using Rockclimbing.BLL;
using Rockclimbing.Controllers.Mappers;
using Rockclimbing.Controllers.APIModel;
using Rockclimbing.BLL.Service;

namespace Rockclimbing.Controllers
{
    public class RouteController : ApiController
    {

        private RouteService route = new RouteService();
        RouteMapper mapper = new RouteMapper();

        [HttpGet]
        [Route("Routes/")]
        [Authorize(Roles = "Admin, Client")]
        public HttpResponseMessage Get()
        {
            List<RouteModel> routes;
            routes = this.route.GetRoutes();
            var json = JsonConvert.SerializeObject(routes);

            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(json)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpGet]
        [Route("Routes/{id}")]
        [Authorize(Roles = "Admin, Client")]
        public List<RouteModel> GetById(int id)
        {
            List<RouteModel> routes = new List<RouteModel>();
            routes = this.route.GetRoutes();
            var filtered = routes.Where(x => x.Id.Equals(id)).ToList();
            return filtered;
        }

        // POST: api/Routes
        [HttpPost]
        [Route("Routes/")]
        [Authorize(Roles = "Admin")]
        public void Post([FromBody]RouteAPI r)
        {
            var converted = mapper.rotMapper(r);
            this.route.Add(converted);
        }

        [HttpPut]
        [Route("Routes/")]
        [Authorize(Roles = "Admin")]
        // PUT: api/Routes/5
        public void Put([FromBody]RouteModel r)
        {
            this.route.Update(r);
        }

        [HttpDelete]
        [Route("Routes/{id}")]
        [Authorize(Roles = "Admin")]
        // DELETE: api/Routes/5
        public void Delete(int id)
        {
            this.route.Delete(id);
        }
    }
}

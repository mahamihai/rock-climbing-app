﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Rockclimbing.Models;
using Rockclimbing.BLL;
using Rockclimbing.Controllers.Mappers;
using Rockclimbing.Controllers.APIModel;
using Rockclimbing.BLL.Service;


namespace Rockclimbing.Controllers
{
    public class MountainRangeController : ApiController
    {
        private MountainRangeService mountain = new MountainRangeService();
        MountainRangeMapper mapper = new MountainRangeMapper();



        [HttpGet]
        [Route("MountainRange/")]
        [Authorize(Roles = "Admin, Client")]
        public List<MountainRangeModel> Get()
        {
            List<MountainRangeModel> mountains = new List<MountainRangeModel>();
            mountains = this.mountain.getMountains();
            return mountains;
        }

        [HttpGet]
        [Route("MountainRange/{id}")]
        [Authorize(Roles = "Admin, Client")]
        public List<MountainRangeModel> GetById(int id)
        {
            List<MountainRangeModel> mountains = new List<MountainRangeModel>();
            mountains = this.mountain.getMountains();
            var filtered = mountains.Where(x => x.Id.Equals(id)).ToList();
            return filtered;
        }

        // POST: api/Mountains
        [HttpPost]
        [Route("MountainRange/")]
        [Authorize(Roles = "Admin")]
        public void Post([FromBody]MountainRangeAPI mtn)
        {
            var converted = mapper.MntMapper(mtn);
            this.mountain.Add(converted);
        }

        [HttpPut]
        [Route("MountainRange/")]
        [Authorize(Roles = "Admin")]
        // PUT: api/Mountains/5
        public void Put([FromBody]MountainRangeModel mtn)
        {
            this.mountain.Update(mtn);
        }

        [HttpDelete]
        [Route("MountainRange/{id}")]
        [Authorize(Roles = "Admin")]
        // DELETE: api/Mountains/5
        public void Delete(int id)
        {
            this.mountain.Delete(id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Rockclimbing.BLL;
using Rockclimbing.Controllers.Mappers;
using Rockclimbing.Controllers.APIModel;
using Rockclimbing.Models;
using Rockclimbing.BLL.Service;

namespace Rockclimbing.Controllers
{
    public class UserController : ApiController
    {
        private UserService user = new UserService();
        UserMapper userMap = new UserMapper();

        // POST: api/User
        public void Post([FromBody]UserAPI user)
        {
            var converted = userMap.UssMapper(user);
            this.user.Add(converted);
        }

        [Route("Users/")]
        [HttpGet]
        public List<UserModel> GetUsers()
        {
            var users = this.user.GetUsers();
            return users;
        }

        [Route("Users/")]
        [HttpPut]
        public void Put([FromBody]UserModel user)
        {
            this.user.Update(user);
        }

        // DELETE: api/User/5
        [Route("Users/{Id}")]
        [HttpDelete]
        public void Delete(int Id)
        {
            this.user.Delete(Id);
        }

        //LogIn
        [HttpGet]
        [Route("Users/login")]
        public UserModel LogIn(string uss, string pass)
        {
            var t = this.user.CheckUser(uss, pass);
            return t;
        }

        //CreateUsser
        [HttpPost]
        [Route("Users/{email}")]
        [Authorize(Roles = "Admin")]
        public string CreateUser(string email)
        {
            return this.user.CreateUser("Client", email);
        }

        //SignUp
        [HttpPut]
        [Route("Users/{token}/{name}/{pass}")]
        public void SignUp(string token, string name, string pass)
        {
            this.user.SignUp(token, name, pass);
        }
    }
}

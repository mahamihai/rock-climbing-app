﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rockclimbing.Controllers.APIModel
{
    public class RouteAPI
    {
        public string Name { get; set; }
        public string Difficulty { get; set; }
        public byte[] Topo { get; set; }
        public string description { get; set; }
        public int ArealId { get; set; }

    }
}
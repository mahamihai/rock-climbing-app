﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rockclimbing.Controllers.APIModel
{
    public class ArealAPI
    {

        public string Name { get; set; }
        public int Mountain_Id { get; set; }
        public string Description { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rockclimbing.Controllers.APIModel
{
    public class WarningAPI
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool MarkedAsResolved { get; set; }
        public int RouteId { get; set; }
        public string DangerLever { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Rockclimbing.Models;
using Rockclimbing.BLL;
using Rockclimbing.Controllers.Mappers;
using Rockclimbing.Controllers.APIModel;
using Rockclimbing.BLL.Service;

namespace Rockclimbing.Controllers
{
    public class WarningController : ApiController
    {

        private WarningService warning = new WarningService();
        WarningMapper mapper = new WarningMapper();

        [HttpGet]
        [Route("Warnings/")]
        [Authorize(Roles = "Admin, Client")]
        public List<WarningModel> Get()
        {
            List<WarningModel> warnings = new List<WarningModel>();
            warnings = this.warning.GetWarnings();
            return warnings;
        }

        [HttpGet]
        [Route("Warnings/{id}")]
        [Authorize(Roles = "Admin, Client")]
        public List<WarningModel> GetById(int id)
        {
            List<WarningModel> warnings = new List<WarningModel>();
            warnings = this.warning.GetWarnings();
            var filtered = warnings.Where(x => x.Id.Equals(id)).ToList();
            return filtered;
        }

        // POST: api/Warnings
        [HttpPost]
        [Route("Warnings/")]
        [Authorize(Roles = "Admin,Client")]
        public void Post([FromBody]WarningAPI w)
        {
            var converted = mapper.WarMapper(w);
            this.warning.Add(converted);
        }

        [HttpPut]
        [Route("Warnings/")]
        [Authorize(Roles = "Admin,Client")]
        // PUT: api/Warnings/5
        public void Put([FromBody]WarningModel w)
        {
            this.warning.Update(w);
        }

        [HttpDelete]
        [Route("Warnings/{id}")]
        [Authorize(Roles = "Admin")]
        // DELETE: api/Warnings/5
        public void Delete(int id)
        {
            this.warning.Delete(id);
        }
    }
}

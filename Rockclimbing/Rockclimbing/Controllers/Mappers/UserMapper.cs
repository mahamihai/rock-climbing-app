﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Rockclimbing.Models;
using Models;
using Rockclimbing.Controllers.APIModel;

namespace Rockclimbing.Controllers.Mappers
{
    public class UserMapper
    {
        public UserModel UssMapper(User uss)
        {
            return new UserModel()
            {
                Id = uss.Id,
                Name = uss.Name,
                Email = uss.Email,
                Password = uss.Password,
                Token = uss.Token,
                Type = uss.Type
            };

        }

        public User UssMapper(UserModel uss)
        {
            return new User()
            {
                Id = uss.Id,
                Name = uss.Name,
                Email = uss.Email,
                Password = uss.Password,
                Token = uss.Token,
                Type = uss.Type
            };
        }

        public UserModel UssMapper(UserAPI uss)
        {
            return new UserModel()
            {
                Name = uss.Name,
                Email = uss.Email,
                Password = uss.Password,
                Token = uss.Token,
                Type = uss.Type
            };
        }
    }
}
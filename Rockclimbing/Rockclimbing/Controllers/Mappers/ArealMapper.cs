﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Rockclimbing.Models;
using Models;
using Rockclimbing.Controllers.APIModel;

namespace Rockclimbing.Controllers.Mappers
{
    public class ArealMapper
    {
        public ArealModel AreMapper(Areal arr)
        {
            return new ArealModel()
            {
                Id = arr.Id,
                Name = arr.Name,
                Description=arr.Description,
               
                Mountain_Id = arr.Mountain_Id
                
            };
        }

        public Areal AreMapper(ArealModel arr)
        {
            return new Areal()
            {
                Id = arr.Id,
                Name = arr.Name,
                Description = arr.Description,


                Mountain_Id = arr.Mountain_Id

            };
        }

        public ArealModel AreMapper(ArealAPI arr)
        {
            return new ArealModel()
            {
               
                Name = arr.Name,
                Description = arr.Description,


                Mountain_Id = arr.Mountain_Id

            };
        }
    }
}
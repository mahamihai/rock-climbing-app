﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Rockclimbing.Models;
using Models;
using Rockclimbing.Controllers.APIModel;

namespace Rockclimbing.Controllers.Mappers
{
    public class WarningMapper
    {
        public WarningModel WarMapper(Warning war)
        {
            return new WarningModel()
            {
                Id = war.Id,
                Description = war.Description,
                MarkedAsResolved=war.MarkedAsResolved,
                RouteId=war.RouteId,
                DangerLever=war.DangerLever
            };
        }

        public Warning WarMapper(WarningModel war)
        {
            return new Warning()
            {
                Id = war.Id,
                Description = war.Description,
                MarkedAsResolved = war.MarkedAsResolved,
                RouteId = war.RouteId,
                DangerLever = war.DangerLever
            };
      
        }

        public WarningModel WarMapper(WarningAPI war)
        {
            return new WarningModel()
            {
                Id = war.Id,
                Description = war.Description,
                MarkedAsResolved = war.MarkedAsResolved,
                RouteId = war.RouteId,
                DangerLever = war.DangerLever
            };

        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Rockclimbing.Models;
using Models;
using Rockclimbing.Controllers.APIModel;

namespace Rockclimbing.Controllers.Mappers
{
    public class RouteMapper
    {
        public RouteModel rotMapper(Route rou)
        {
            return new RouteModel()
            {
                Id = rou.Id,
                Name = rou.Name,
                Difficulty = rou.Difficulty,
              
                Topo = rou.Topo,
                description = rou.description,
                ArealId=rou.ArealId
            };
        }

        public Route rotMapper(RouteModel rou)
        {
            return new Route()
            {
                Id = rou.Id,
                Name = rou.Name,
                Difficulty = rou.Difficulty,

                Topo = rou.Topo,
                description = rou.description,
                ArealId = rou.ArealId

            };
        }

        public RouteModel rotMapper(RouteAPI rou)
        {
            return new RouteModel()
            {
              
                Name = rou.Name,
                Difficulty = rou.Difficulty,

                Topo = rou.Topo,
                description = rou.description,
                ArealId = rou.ArealId

            };
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Rockclimbing.Models;
using Models;
using Rockclimbing.Controllers.APIModel;

namespace Rockclimbing.Controllers.Mappers
{
    public class MountainRangeMapper
    {
        public MountainRangeModel MntMapper(Mountain mtn)
        {
            return new MountainRangeModel()
            {
                Id = mtn.Id,
                Name = mtn.Name,
               Description=mtn.Description
            };
        }

        public Mountain MntMapper(MountainRangeModel mtn)
        {
            return new Mountain()
            {
                Id = mtn.Id,
                Name = mtn.Name,
                Description = mtn.Description
            };
        }

        public MountainRangeModel MntMapper(MountainRangeAPI mtn)
        {
            return new MountainRangeModel()
            {
                Id = mtn.Id,
                Name = mtn.Name,
                Description = mtn.Description
            };
        }
    }
}
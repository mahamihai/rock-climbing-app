﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models.Repo;
using Rockclimbing.Controllers.Mappers;
using Models.IRepo;
using Rockclimbing.Models;

namespace Rockclimbing.BLL.Service
{
    public class MountainRangeService
    {
        private IMountainRangeRepo mountainR;

        public MountainRangeService()
        {
            mountainR = new MountainRangeRepo();
        }

        public void Add(MountainRangeModel mountain)
        {
            MountainRangeMapper mapper = new MountainRangeMapper();
            var toBeUpdated = mapper.MntMapper(mountain);
            mountainR.Add(toBeUpdated);
        }

        public void Delete(int id)
        {
            mountainR.Delete(id);
        }

        public void Update(MountainRangeModel mountain)
        {
            MountainRangeMapper mapper = new MountainRangeMapper();
            var toBeUpdated = mapper.MntMapper(mountain);
            mountainR.Update(toBeUpdated);
        }

        public List<MountainRangeModel> getMountains()
        {
            var mountains = this.mountainR.GetAll();
            MountainRangeMapper mapper = new MountainRangeMapper();
            var converted = mountains.Select(x => mapper.MntMapper(x)).ToList();
            return converted;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models.Repo;
using Rockclimbing.Controllers.Mappers;
using Models.IRepo;
using Rockclimbing.Models;
using Rockclimbing.Controllers.Mappers;

namespace Rockclimbing.BLL.Service
{
    public class ArealService
    {
        private IArealRepo arealR;

        public ArealService()
        {
            arealR = new ArealRepo();
        }

        public void Add(ArealModel areal)
        {
            ArealMapper mapper = new ArealMapper();
            var toBeUpdated = mapper.AreMapper(areal);
            arealR.Add(toBeUpdated);
        }

        public void Delete(int id)
        {
            arealR.Delete(id);
        }

        public void Update(ArealModel areal)
        {
            ArealMapper mapper = new ArealMapper();
            var toBeUpdated = mapper.AreMapper(areal);
            arealR.Update(toBeUpdated);
        }

        public List<ArealModel> getAreals()
        {
            var areals = this.arealR.GetAll();
            ArealMapper mapper = new ArealMapper();
            var converted = areals.Select(x => mapper.AreMapper(x)).ToList();
            return converted;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models.Repo;
using Rockclimbing.Controllers.Mappers;
using Models.IRepo;
using Rockclimbing.Models;


namespace Rockclimbing.BLL.Service
{
    public class RouteService
    {
        private IRouteRepo routeR;

        public RouteService()
        {
            routeR = new RouteRepo();
        }

        public void Add(RouteModel route)
        {
            RouteMapper mapper = new RouteMapper();
            var toBeAdded = mapper.rotMapper(route);
            routeR.Add(toBeAdded);
        }

        public void Delete(int id)
        {
            routeR.Delete(id);
        }

        public void Update(RouteModel route)
        {
            RouteMapper mapper = new RouteMapper();
            var toBeUpdated = mapper.rotMapper(route);
            routeR.Update(toBeUpdated);
        }

        public List<RouteModel> GetRoutes()
        {
            var routes = this.routeR.GetAll();
            RouteMapper mapper = new RouteMapper();
            var converted = routes.Select(x => mapper.rotMapper(x)).ToList();
            return converted;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models.Repo;
using Rockclimbing.Controllers.Mappers;
using Models.IRepo;
using Rockclimbing.Models;
using Rockclimbing.Controllers.Mappers;



namespace Rockclimbing.BLL.Service
{
    public class UserService
    {
        private static Random random = new Random();

        public static string RandomString()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, 10)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private IUserRepo ussRepo;

        public UserService()
        {
            ussRepo = new UserRepo();
        }

        public void Add(UserModel user)
        {
            UserMapper mapper = new UserMapper();
            var toBeUpdated = mapper.UssMapper(user);
            ussRepo.Add(toBeUpdated);
        }

        public void Delete(int Id)
        {
            ussRepo.Delete(Id);
        }

        public void Update(UserModel user)
        {
            UserMapper mapper = new UserMapper();
            var toBeUpdated = mapper.UssMapper(user);
            ussRepo.Update(toBeUpdated);
        }

        public List<UserModel> GetUsers()
        {
            var students = this.ussRepo.GetAll();
            UserMapper mapper = new UserMapper();
            var converted = students.Select(x => mapper.UssMapper(x)).ToList();
            return converted;
        }

        public UserModel CheckUser(string uss, string pass)
        {
            var rez = ussRepo.CheckUser(uss, pass);
            UserMapper usr = new UserMapper();
            if (rez != null)
            {
                return usr.UssMapper(rez);
            }
            else
            {
                return null;
            }
        }

        public string CreateUser(string type, string email)
        {
            string token = RandomString();
            ussRepo.CreateUser(type, token, email);
            (new EmailFactory()).sendEmail(email, token);
            return token;
        }

        public void SignUp(string token, string name, string pass)
        {
            ussRepo.SignUp(token, name, pass);
        }
    }
}
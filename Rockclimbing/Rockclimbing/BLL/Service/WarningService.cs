﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models.Repo;
using Rockclimbing.Controllers.Mappers;
using Models.IRepo;
using Rockclimbing.Models;

namespace Rockclimbing.BLL.Service
{
    public class WarningService
    {
        private IWarningRepo warningR;

        public WarningService()
        {
            warningR = new WarningRepo();
        }

        public void Add(WarningModel warning)
        {
            WarningMapper mapper = new WarningMapper();
            var toBeAdded = mapper.WarMapper(warning);
            warningR.Add(toBeAdded);
        }

        public void Delete(int id)
        {
            warningR.Delete(id);
        }

        public void Update(WarningModel warning)
        {
            WarningMapper mapper = new WarningMapper();
            var toBeUpdated = mapper.WarMapper(warning);
            warningR.Update(toBeUpdated);
        }

        public List<WarningModel> GetWarnings()
        {
            var warnings = this.warningR.GetAll();
            WarningMapper mapper = new WarningMapper();
            var converted = warnings.Select(x => mapper.WarMapper(x)).ToList();
            return converted;
        }
    }
}
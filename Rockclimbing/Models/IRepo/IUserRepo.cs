﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.IRepo
{
    public interface IUserRepo
    {
        void Add(User war);

        void Delete(int id);

        void Update(User rou);

        List<User> GetAll();

        User CheckUser(string uss, string pass);

        void CreateUser(string type, string token, string email);

        void SignUp(string token, string name, string pass);
    }
}

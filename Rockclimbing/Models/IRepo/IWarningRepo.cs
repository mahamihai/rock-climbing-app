﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.IRepo
{
    public interface IWarningRepo
    {
        void Add(Warning war);

        void Delete(int id);

        void Update(Warning rou);

        List<Warning> GetAll();

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.IRepo
{
    public interface IRouteRepo
    {
        void Add(Route war);

        void Delete(int id);

        void Update(Route rou);

        List<Route> GetAll();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Models.IRepo
{
    public interface IMountainRangeRepo
    {
        void Add(Mountain war);

        void Delete(int id);

        void Update(Mountain rou);

        List<Mountain> GetAll();
    }
}

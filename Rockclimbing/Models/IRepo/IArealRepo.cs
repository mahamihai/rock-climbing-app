﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
namespace Models.IRepo
{
    public interface IArealRepo
    {
        void Add(Areal war);

        void Delete(int id);

        void Update(Areal rou);

        List<Areal> GetAll();
    }
}

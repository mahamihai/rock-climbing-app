﻿using Models.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Repo
{
    public class RouteRepo : IRouteRepo
    {
        public RockclimbingEntities context;

        public RouteRepo()
        {
            context = new RockclimbingEntities();
        }

        public void Add(Route rou)
        {
            context.Set<Route>().Add(rou);
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var toBeDeleted = context.Set<Route>().Find(id);
            context.Set<Route>().Remove(toBeDeleted);
            context.SaveChanges();
        }

        public void Update(Route rou)
        {
            var toBeUpdated = context.Set<Route>().Find(rou.Id);
            context.Entry(toBeUpdated).CurrentValues.SetValues(rou);
            context.SaveChanges();
        }

        public List<Route> GetAll()
        {
            return context.Set<Route>().ToList<Route>();
        }

      
    }
}

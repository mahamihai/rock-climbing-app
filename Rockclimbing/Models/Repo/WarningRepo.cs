﻿using Models.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Repo
{
    public class WarningRepo : IWarningRepo
    {
        public RockclimbingEntities context;
       
        private IWarningRepo warRepo;

        public WarningRepo()
        {
            context = new RockclimbingEntities();
        }

        public void Add(Warning war)
        {
            context.Set<Warning>().Add(war);
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var toBeDeleted = context.Set<Warning>().Find(id);
            context.Set<Warning>().Remove(toBeDeleted);
            context.SaveChanges();
        }

        public void Update(Warning rou)
        {
            var toBeUpdated = context.Set<Warning>().Find(rou.Id);
            context.Entry(toBeUpdated).CurrentValues.SetValues(rou);
            context.SaveChanges();
        }

        public List<Warning> GetAll()
        {
            return context.Set<Warning>().ToList<Warning>();
        }

    }
}

﻿using Models.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Repo
{
    public class UserRepo : IUserRepo
    {
        public RockclimbingEntities context;

        public UserRepo()
        {
            context = new RockclimbingEntities();
        }

        public void Add(User uss)
        {
            context.Set<User>().Add(uss);
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var toBeDeleted = context.Set<User>().Find(id);
            context.Set<User>().Remove(toBeDeleted);
            context.SaveChanges();
        }

        public void Update(User uss)
        {
            var toBeUpdated = context.Set<User>().Find(uss.Id);
            context.Entry(toBeUpdated).CurrentValues.SetValues(uss);
            context.SaveChanges();
        }

        public List<User> GetAll()
        {
            return context.Set<User>().ToList<User>();
        }

        public User CheckUser(string uss, string pass)
        {
            var check = context.Set<User>().FirstOrDefault(x => x.Password.Equals(pass) && x.Email.Equals(uss));

            if (check == null)
            {
                return null;
            }
            else
            {
                return check;
            }

        }

        public void CreateUser(string type, string token, string email)
        {
            User user = new User
            {
               Type = type,
               Token = token,
               Email = email,
            };

            context.Set<User>().Add(user);
            context.SaveChanges();
        }

        public void SignUp(string token, string name, string pass)
        {
            var toBeUpdated = context.Set<User>().Where(x => x.Token.Equals(token)).FirstOrDefault();

            if (toBeUpdated != null)
            {
                toBeUpdated.Name = name;
                toBeUpdated.Password = pass;
                toBeUpdated.Token = "";
                context.SaveChanges();
            }
        }
    }
}

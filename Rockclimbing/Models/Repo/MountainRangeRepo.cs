﻿using Models.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Repo
{
    public class MountainRangeRepo : IMountainRangeRepo

    {
        public RockclimbingEntities context;

        public MountainRangeRepo()
        {
            context = new RockclimbingEntities();
        }

        public void Add(Mountain mtn)
        {
            context.Set<Mountain>().Add(mtn);
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var toBeDeleted = context.Set<Mountain>().Find(id);
            context.Set<Mountain>().Remove(toBeDeleted);
            context.SaveChanges();
        }

        public void Update(Mountain mtn)
        {
            var toBeUpdated = context.Set<Mountain>().Find(mtn.Id);
            context.Entry(toBeUpdated).CurrentValues.SetValues(mtn);
            context.SaveChanges();
        }

        public List<Mountain> GetAll()
        {
            return context.Set<Mountain>().ToList<Mountain>();
        }

       
    }
}

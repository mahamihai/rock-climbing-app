﻿using Models.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Repo
{
    public class ArealRepo : IArealRepo
    {
        public RockclimbingEntities context;

        public ArealRepo()
        {
            context = new RockclimbingEntities();
        }

        public void Add(Areal are)
        {
            context.Set<Areal>().Add(are);
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var toBeDeleted = context.Set<Areal>().Find(id);
            context.Set<Areal>().Remove(toBeDeleted);
            context.SaveChanges();
        }

        public void Update(Areal are)
        {
            var toBeUpdated = context.Set<Areal>().Find(are.Id);
            context.Entry(toBeUpdated).CurrentValues.SetValues(are);
            context.SaveChanges();
        }

        public List<Areal> GetAll()
        {
            return context.Set<Areal>().ToList<Areal>();
        }

       
    }
}

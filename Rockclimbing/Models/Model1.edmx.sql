
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/31/2018 21:25:13
-- Generated from EDMX file: F:\An3Sem2\SD\Peter\Final\rock-climbing-app\Rockclimbing\Models\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Rockclimbing];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Areals]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Areals];
GO
IF OBJECT_ID(N'[dbo].[Mountain]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Mountain];
GO
IF OBJECT_ID(N'[dbo].[Routes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Routes];
GO
IF OBJECT_ID(N'[dbo].[sysdiagrams]', 'U') IS NOT NULL
    DROP TABLE [dbo].[sysdiagrams];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[Warnings]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Warnings];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Areals'
CREATE TABLE [dbo].[Areals] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Mountain_Id] int  NOT NULL,
    [Description] nvarchar(50)  NULL
);
GO

-- Creating table 'Mountains'
CREATE TABLE [dbo].[Mountains] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Description] nvarchar(50)  NULL
);
GO

-- Creating table 'Routes'
CREATE TABLE [dbo].[Routes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Difficulty] nvarchar(50)  NULL,
    [Topo] varbinary(max)  NULL,
    [description] nvarchar(200)  NULL,
    [ArealId] int  NOT NULL
);
GO

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Type] nvarchar(50)  NOT NULL,
    [Name] nvarchar(50)  NULL,
    [Email] nvarchar(50)  NOT NULL,
    [Password] nvarchar(50)  NULL,
    [Token] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'Warnings'
CREATE TABLE [dbo].[Warnings] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Description] nvarchar(200)  NULL,
    [MarkedAsResolved] bit  NOT NULL,
    [RouteId] int  NOT NULL,
    [DangerLever] nvarchar(50)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Areals'
ALTER TABLE [dbo].[Areals]
ADD CONSTRAINT [PK_Areals]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Mountains'
ALTER TABLE [dbo].[Mountains]
ADD CONSTRAINT [PK_Mountains]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Routes'
ALTER TABLE [dbo].[Routes]
ADD CONSTRAINT [PK_Routes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [Id], [Email] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id], [Email] ASC);
GO

-- Creating primary key on [Id] in table 'Warnings'
ALTER TABLE [dbo].[Warnings]
ADD CONSTRAINT [PK_Warnings]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------
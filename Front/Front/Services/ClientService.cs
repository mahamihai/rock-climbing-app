﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using Front.Models;
using Front.Models.API;
using Newtonsoft.Json;

namespace Front.Services
{
   public class ClientService
    {
        private RestClient _client;

        public ClientService(RestClient client)
        {
            this._client = client;
        }

        public List<MountainRangeModel> GetMountains()
        {
            var request = new RestRequest("MountainRange/", Method.GET);
            var response = _client.Execute<List<MountainRangeModel>>(request).Data;
            return response;
        }

        public List<ArealModel> GetAreals()
        {
            var request = new RestRequest("Areals/", Method.GET);
            var response = _client.Execute<List<ArealModel>>(request).Data;
            return response;
        }

        public List<ArealModel> GetArealsById(int id)
        {
            var allAreals = this.GetAreals();
            var filtered = allAreals.Where(x => x.Mountain_Id.Equals(id)).ToList();
            return filtered;
        }

        public List<RouteModel> GetRoutes()
        {
            var request = new RestRequest("Routes/", Method.GET);
            var response = _client.Execute(request);
            var data = response.Content;
            List<RouteModel> tmp = JsonConvert.DeserializeObject<List<RouteModel>>(data);
            return tmp;
        }
        public List<RouteModel> GetRoutesByArealId(int arealId)
        {
            var allRoutes = this.GetRoutes();
            var filtered = allRoutes.Where(x => x.ArealId.Equals(arealId)).ToList();
            return filtered;
        }
        public RouteModel GetRouteById(int routeId)
        {
            var allRoutes = this.GetRoutes();
            var route = allRoutes.Find(x => x.Id.Equals(routeId));
            return route;
        }
        public List<WarningModel> getWarnings()
        {
            var request = new RestRequest("Warnings/", Method.GET);
            var response = _client.Execute<List<WarningModel>>(request).Data;
            return response;
        }
        public List<WarningModel> getWarningByRouteId(int routeId)
        {
            var allWarnings = this.GetWarnings();
            var filtered = allWarnings.Where(x => x.RouteId.Equals(routeId)).ToList();
            return filtered;

        }
        public List<WarningModel> GetWarnings()
        {
            var request = new RestRequest("Warnings/", Method.GET);
            var response = _client.Execute<List<WarningModel>>(request).Data;
            return response;
        }

        public string createWarning(WarningAPI newWarning)
        {
            var request = new RestRequest("Warnings/", Method.POST);
            var json = JsonConvert.SerializeObject(newWarning);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            var answer = _client.Execute(request).StatusCode;
            return answer.ToString();
        }

        public string MarkWarningOK(int id)
        {
            var warnings = this.GetWarnings();
            WarningModel selectedWarning = warnings.Find(x => x.Id.Equals(id));//gaseste eroarea cu idul dat
            selectedWarning.MarkedAsResolved = true;//marcheaza ca vizitata

            var request = new RestRequest("Warnings/", Method.PUT);
            var json = JsonConvert.SerializeObject(selectedWarning);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            var answer = _client.Execute(request).StatusCode;
            return answer.ToString();
        }
    }
}

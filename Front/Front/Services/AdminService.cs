﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using Front.Models;
using Front.Models.API;
using Newtonsoft.Json;
using RestSharp;
using Front.Models.API;

namespace Front.Services
{
   public class AdminService
    {
        private RestClient _client;

        public AdminService(RestClient client)
        {
            this._client = client;
        }


        //Mountain Range.......................................................................
        public List<MountainRangeModel> GetMountains()
        {
            var request = new RestRequest("MountainRange/", Method.GET);
            var response = _client.Execute<List<MountainRangeModel>>(request).Data;
            return response;
        }

        public string CreateMountainRange(MountainRangeAPI mtn)
        {
            var request = new RestRequest("MountainRange/", Method.POST);
            var json = JsonConvert.SerializeObject(mtn);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            var answer = _client.Execute(request).StatusCode;
            return answer.ToString();
        }

        public string UpdateMountainRange(MountainRangeModel mtn)
        {
            var request = new RestRequest("MountainRange/", Method.PUT);
            var json = JsonConvert.SerializeObject(mtn);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            //request.AddJsonBody( json);

            var answer = _client.Execute(request).StatusCode;
            return answer.ToString();
        }

        public string DeleteMountainRange(object id)
        {
            var request = new RestRequest("MountainRange/{id}", Method.DELETE);
            request.AddParameter("id", id);
            var answer = _client.Execute(request).StatusCode;
            return answer.ToString();
        }

        //Areal.........................................................................

        public List<ArealModel> GetAreals()
        {
            var request = new RestRequest("Areals/", Method.GET);
            var response = _client.Execute<List<ArealModel>>(request).Data;
            return response;
        }

        public string CreateAreal(ArealAPI are)
        {
            var request = new RestRequest("Areals/", Method.POST);
            var json = JsonConvert.SerializeObject(are);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            var answer = _client.Execute(request).StatusCode;
            return answer.ToString();
        }
        public List<ArealModel> GetArealsById(int id)
        {
            var allAreals = this.GetAreals();
            var filtered = allAreals.Where(x => x.Mountain_Id.Equals(id)).ToList();
            return filtered;
        }
        public string UpdateAreal(ArealModel are)
        {
            var request = new RestRequest("Areals/", Method.PUT);
            var json = JsonConvert.SerializeObject(are);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            var answer = _client.Execute(request).StatusCode;
            return answer.ToString();
        }

        public string DeleteAreal(object id)
        {
            var request = new RestRequest("Areals/{id}", Method.DELETE);
            request.AddParameter("id", id);
            var answer = _client.Execute(request).StatusCode;
            return answer.ToString();
        }

        //Routes...........................................................................

        public List<RouteModel> GetRoutes()
        {
            var request = new RestRequest("Routes/", Method.GET);
            var response = _client.Execute(request);
            var data = response.Content;
            List<RouteModel> tmp = JsonConvert.DeserializeObject<List<RouteModel>>(data);
            return tmp;
        }

        public string CreateRoute(RouteAPI rou)
        {
            var request = new RestRequest("Routes/", Method.POST);
            var json = JsonConvert.SerializeObject(rou);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            var answer = _client.Execute(request).StatusCode;
            return answer.ToString();
        }

        public string UpdateRoute(RouteModel rou)
        {
            var request = new RestRequest("Routes/", Method.PUT);
            var json = JsonConvert.SerializeObject(rou);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            var answer = _client.Execute(request).StatusCode;
            return answer.ToString();
        }

        public string DeleteRoute(object id)
        {
            var request = new RestRequest("Routes/{id}", Method.DELETE);
            request.AddParameter("id", id);
            var answer = _client.Execute(request).StatusCode;
            return answer.ToString();
        }

        public List<RouteModel> GetRoutesByArealId(int arealId)
        {
            var allRoutes = this.GetRoutes();
            var filtered = allRoutes.Where(x => x.ArealId.Equals(arealId)).ToList();
            return filtered;
        }

        //Warning............................................................................

        public List<WarningModel> GetWarnings()
        {
            var request = new RestRequest("Warnings/", Method.GET);
            var response = _client.Execute<List<WarningModel>>(request).Data;
            return response;
        }

        public string createWarning(WarningAPI newWarning)
        {
            var request = new RestRequest("Warnings/", Method.POST);
            var json = JsonConvert.SerializeObject(newWarning);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            var answer = _client.Execute(request).StatusCode;
            return answer.ToString();
        }

        public string UpdateWarning(WarningAPI war)
        {
            var request = new RestRequest("Warnings/", Method.PUT);
            var json = JsonConvert.SerializeObject(war);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            var answer = _client.Execute(request).StatusCode;
            return answer.ToString();
        }

        public string DeleteWarning(object id)
        {
            var request = new RestRequest("Warnings/", Method.DELETE);
            request.AddParameter("id", id);
            var answer = _client.Execute(request).StatusCode;
            return answer.ToString();
        }

        public string MarkWarningOK(int id)
        {
            var warnings = this.GetWarnings();
            WarningModel selectedWarning = warnings.Find(x => x.Id.Equals(id));//gaseste eroarea cu idul dat
            selectedWarning.MarkedAsResolved = true;//marcheaza ca vizitata

            var request = new RestRequest("Warning/", Method.PUT);
            var json = JsonConvert.SerializeObject(selectedWarning);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            var answer = _client.Execute(request).StatusCode;
            return answer.ToString();
        }

        public string CreateClient(string email)
        {

            var request = new RestRequest("Users/{email}", Method.POST);
           request.AddQueryParameter("email",email);
            var answer = _client.Execute(request).StatusCode;
            return answer.ToString();
        }


    }
}

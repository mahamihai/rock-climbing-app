﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Front.Models;
using Front.Services;
using Front.Views;
using RestSharp;
using RestSharp.Authenticators;

namespace Front.Services
{
    public class LoginService
    {
        private RestClient _client;

        public LoginService()
        {
            this._client = new RestClient("http://localhost:50315");
        }

        public UserModel checkClient(string username, string password)
        {
            RestRequest request = new RestRequest("Users/login", Method.GET);
            request.AddParameter("uss", username);
            request.AddParameter("pass", password);
            _client.Authenticator = new HttpBasicAuthenticator(username, password);

            var response = _client.Execute<UserModel>(request).Data;
            if (response != null)
            {
                switch (response.Type)
                {
                    case "Admin":
                        _client.Authenticator = new HttpBasicAuthenticator(username, password);
                        var adminServices = new AdminService(this._client);
                        AdminForm aView = new AdminForm(adminServices);
                        aView.Show();
                        break;

                    case "Client":
                        _client.Authenticator = new HttpBasicAuthenticator(username, password);
                        var clientSevice = new ClientService(this._client);
                        ClientForm cForm = new ClientForm(clientSevice);
                        cForm.Show();
                        break;

                    default:
                        MessageBox.Show("Something went rong.");
                        break;
                }
            }
            else
            {
                MessageBox.Show("Username or password is incorrect.");
            }
            return response;
        }

        public void Register(string token, string email, string password)
        {
            RestRequest request = new RestRequest("Users/{token}/{name}/{pass}", Method.PUT);
            request.AddQueryParameter("token", token);
            request.AddQueryParameter("name", email);
            request.AddQueryParameter("pass", password);
            var t = _client.Execute(request).StatusCode;
        }
    }
}

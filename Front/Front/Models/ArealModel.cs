﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Front.Models
{
    public class ArealModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Mountain_Id { get; set; }
        public string Description { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Front.Models
{
    public class RouteModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Difficulty { get; set; }
        public byte[] Topo { get; set; }
        public string description { get; set; }
        public int ArealId { get; set; }

        public RouteModel()
        {
            
        }
    }
 
}
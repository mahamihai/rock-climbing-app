﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Front.Models.API
{
    public class MountainRangeAPI
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Front.Models;
using Front.Services;
using Front.Models.API;
namespace Front.Views
{
    public partial class AdminForm : Form
    {
        private AdminService _admin;
        public AdminForm(AdminService adminService)
        {
            InitializeComponent();
            this._admin = adminService;
        }

        private int selectedMountainId, selectedArealId, selectedRouteId;
        private string filePath;
        private void button1_Click(object sender, EventArgs e)
        {
            string name = this.textBox1.Text;
            string description = this.textBox2.Text;

            MountainRangeAPI newMount = new MountainRangeAPI
            {
                Name = name,
                Description = description
            };
            this._admin.CreateMountainRange(newMount);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string name = this.textBox1.Text;
            string description = this.textBox2.Text;

            MountainRangeModel newMount = new MountainRangeModel
            {
                Id = this.selectedMountainId,
                Name = name,
                Description = description
            };
            this._admin.UpdateMountainRange(newMount);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var mountains = this._admin.GetMountains();
            this.dataGridView1.DataSource = mountains;
        }

        private void AdminForm_Load(object sender, EventArgs e)
        {

        }

        private void button13_Click(object sender, EventArgs e)
        {
            OpenFileDialog theDialog = this.openFileDialog1;
            theDialog.Title = "Open Image File";
            theDialog.Filter = "Img files|*.jpg|*.bmp|*.png";
            theDialog.InitialDirectory = @"C:\";
            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                MessageBox.Show(theDialog.FileName.ToString());
                this.filePath = theDialog.FileName.ToString();
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            string name = this.textBox5.Text;
            string diffuculty = this.textBox6.Text;
            string description = this.textBox7.Text;
            Image img = Image.FromFile(this.filePath);
            byte[] imgArray = this.imageToByteArray(img);
            RouteAPI newRoute = new RouteAPI
            {
                ArealId = this.selectedArealId,
                description = description,
                Difficulty = diffuculty,
                Name = name,
                Topo = imgArray


            };
            this._admin.CreateRoute(newRoute);

        }
        private byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            string name = this.textBox3.Text;
            string description = this.textBox4.Text;
            ArealAPI newAreal = new ArealAPI
            {
                Name = name,
                Description = description,
                Mountain_Id = this.selectedMountainId
            };
            this._admin.CreateAreal(newAreal);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this._admin.DeleteAreal(this.selectedArealId);
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id;

            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView2.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.selectedArealId = id;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string name = this.textBox3.Text;
            string description = this.textBox4.Text;
            ArealModel newAreal = new ArealModel
            {
                Id = this.selectedArealId,
                Name = name,
                Description = description,
                Mountain_Id = this.selectedMountainId
            };
            this._admin.UpdateAreal(newAreal);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var areals = this._admin.GetArealsById(this.selectedMountainId);
            this.dataGridView2.DataSource = areals;

        }

        private void dataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id;

            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView3.Rows[row].Cells["Id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.selectedRouteId = id;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            this._admin.DeleteRoute(this.selectedRouteId);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            string name = this.textBox5.Text;
            string diffuculty = this.textBox6.Text;
            string description = this.textBox7.Text;
            Image img = Image.FromFile(this.filePath);
            byte[] imgArray = this.imageToByteArray(img);
            RouteModel newRoute = new RouteModel
            {
                Id = this.selectedRouteId,
                ArealId = this.selectedArealId,
                description = description,
                Difficulty = diffuculty,
                Name = name,
                Topo = imgArray


            };
            this._admin.UpdateRoute(newRoute);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            var routes = this._admin.GetRoutesByArealId(this.selectedArealId);
            this.dataGridView3.DataSource = routes;
        }

        private void button14_Click(object sender, EventArgs e)
        {
            string email = this.textBox8.Text;
            this._admin.CreateClient(email);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this._admin.DeleteMountainRange(this.selectedMountainId);


        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            int id;

            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView1.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.selectedMountainId = id;
        }
    }
}

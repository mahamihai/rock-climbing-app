﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Front.Services;

namespace Front.Views
{
    public partial class RegisterForm : Form
    {
        public RegisterForm(LoginService login)
        {
            InitializeComponent();
            this._login = login;
        }

        private LoginService _login;

        private void Register_Click(object sender, EventArgs e)
        {
            string token = this.TokenT.Text;
            string name = this.NameT.Text;
            string password = this.PasswordT.Text;

            this._login.Register(token, name, password);

        }
    }
}

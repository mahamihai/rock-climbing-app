﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Front.Services;

namespace Front.Views
{
    public partial class Login : Form
    {
        private LoginService _login;

        public Login()
        {
            InitializeComponent();
            this._login = new LoginService();
        }

        private void LoginBtn_Click(object sender, EventArgs e)
        {
            string email = this.username.Text;
            string password = this.Password.Text;
            var response = this._login.checkClient(email, password);
        }

        private void Register_Click(object sender, EventArgs e)
        {
            RegisterForm reg = new RegisterForm(this._login);
            reg.Show();
        }
    }
}

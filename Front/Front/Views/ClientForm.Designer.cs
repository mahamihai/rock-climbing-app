﻿namespace Front
{
    partial class ClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MountainsGridView = new System.Windows.Forms.DataGridView();
            this.ArealGridView = new System.Windows.Forms.DataGridView();
            this.TrailGridView = new System.Windows.Forms.DataGridView();
            this.ShowAreals = new System.Windows.Forms.Button();
            this.ShowRoutes = new System.Windows.Forms.Button();
            this.SelectRoute = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.CreateWarning = new System.Windows.Forms.Button();
            this.markWarOk = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.MountainsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArealGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrailGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // MountainsGridView
            // 
            this.MountainsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MountainsGridView.Location = new System.Drawing.Point(17, 34);
            this.MountainsGridView.Margin = new System.Windows.Forms.Padding(2);
            this.MountainsGridView.Name = "MountainsGridView";
            this.MountainsGridView.RowTemplate.Height = 28;
            this.MountainsGridView.Size = new System.Drawing.Size(298, 374);
            this.MountainsGridView.TabIndex = 0;
            this.MountainsGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MountainsGridView_CellClick);
            this.MountainsGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MountainsGridView_CellContentClick);
            // 
            // ArealGridView
            // 
            this.ArealGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ArealGridView.Location = new System.Drawing.Point(397, 34);
            this.ArealGridView.Margin = new System.Windows.Forms.Padding(2);
            this.ArealGridView.Name = "ArealGridView";
            this.ArealGridView.RowTemplate.Height = 28;
            this.ArealGridView.Size = new System.Drawing.Size(334, 374);
            this.ArealGridView.TabIndex = 1;
            this.ArealGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ArealGridView_CellClick);
            // 
            // TrailGridView
            // 
            this.TrailGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TrailGridView.Location = new System.Drawing.Point(796, 38);
            this.TrailGridView.Margin = new System.Windows.Forms.Padding(2);
            this.TrailGridView.Name = "TrailGridView";
            this.TrailGridView.RowTemplate.Height = 28;
            this.TrailGridView.Size = new System.Drawing.Size(387, 370);
            this.TrailGridView.TabIndex = 2;
            this.TrailGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TrailGridView_CellClick);
            // 
            // ShowAreals
            // 
            this.ShowAreals.Location = new System.Drawing.Point(17, 421);
            this.ShowAreals.Margin = new System.Windows.Forms.Padding(2);
            this.ShowAreals.Name = "ShowAreals";
            this.ShowAreals.Size = new System.Drawing.Size(147, 25);
            this.ShowAreals.TabIndex = 3;
            this.ShowAreals.Text = "Show Areals";
            this.ShowAreals.UseVisualStyleBackColor = true;
            this.ShowAreals.Click += new System.EventHandler(this.ShowAreals_Click);
            // 
            // ShowRoutes
            // 
            this.ShowRoutes.Location = new System.Drawing.Point(397, 421);
            this.ShowRoutes.Margin = new System.Windows.Forms.Padding(2);
            this.ShowRoutes.Name = "ShowRoutes";
            this.ShowRoutes.Size = new System.Drawing.Size(228, 25);
            this.ShowRoutes.TabIndex = 4;
            this.ShowRoutes.Text = "Show Routes";
            this.ShowRoutes.UseVisualStyleBackColor = true;
            this.ShowRoutes.Click += new System.EventHandler(this.ShowRoutes_Click);
            // 
            // SelectRoute
            // 
            this.SelectRoute.Location = new System.Drawing.Point(796, 423);
            this.SelectRoute.Margin = new System.Windows.Forms.Padding(2);
            this.SelectRoute.Name = "SelectRoute";
            this.SelectRoute.Size = new System.Drawing.Size(151, 25);
            this.SelectRoute.TabIndex = 5;
            this.SelectRoute.Text = "Select Route";
            this.SelectRoute.UseVisualStyleBackColor = true;
            this.SelectRoute.Click += new System.EventHandler(this.SelectRoute_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(23, 535);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 28;
            this.dataGridView1.Size = new System.Drawing.Size(396, 128);
            this.dataGridView1.TabIndex = 6;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Mountain Ranges";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(453, 11);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Areals";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(857, 13);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Routes";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 506);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Warnings on the selcted trail";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(1019, 531);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(153, 20);
            this.textBox1.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(948, 534);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Description:";
            // 
            // CreateWarning
            // 
            this.CreateWarning.Location = new System.Drawing.Point(1021, 599);
            this.CreateWarning.Margin = new System.Windows.Forms.Padding(2);
            this.CreateWarning.Name = "CreateWarning";
            this.CreateWarning.Size = new System.Drawing.Size(151, 29);
            this.CreateWarning.TabIndex = 15;
            this.CreateWarning.Text = "CreateWarning";
            this.CreateWarning.UseVisualStyleBackColor = true;
            this.CreateWarning.Click += new System.EventHandler(this.CreateWarning_Click);
            // 
            // markWarOk
            // 
            this.markWarOk.Location = new System.Drawing.Point(23, 688);
            this.markWarOk.Margin = new System.Windows.Forms.Padding(2);
            this.markWarOk.Name = "markWarOk";
            this.markWarOk.Size = new System.Drawing.Size(136, 48);
            this.markWarOk.TabIndex = 24;
            this.markWarOk.Text = "Mark selected Warning OK";
            this.markWarOk.UseVisualStyleBackColor = true;
            this.markWarOk.Click += new System.EventHandler(this.markWarOk_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(494, 460);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(291, 222);
            this.pictureBox1.TabIndex = 25;
            this.pictureBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(948, 566);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Danger level";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(1019, 566);
            this.textBox2.Margin = new System.Windows.Forms.Padding(2);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(153, 20);
            this.textBox2.TabIndex = 26;
            // 
            // ClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1213, 765);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.markWarOk);
            this.Controls.Add(this.CreateWarning);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.SelectRoute);
            this.Controls.Add(this.ShowRoutes);
            this.Controls.Add(this.ShowAreals);
            this.Controls.Add(this.TrailGridView);
            this.Controls.Add(this.ArealGridView);
            this.Controls.Add(this.MountainsGridView);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "ClientForm";
            this.Text = "ClientForm";
            ((System.ComponentModel.ISupportInitialize)(this.MountainsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArealGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrailGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView MountainsGridView;
        private System.Windows.Forms.DataGridView ArealGridView;
        private System.Windows.Forms.DataGridView TrailGridView;
        private System.Windows.Forms.Button ShowAreals;
        private System.Windows.Forms.Button ShowRoutes;
        private System.Windows.Forms.Button SelectRoute;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button CreateWarning;
        private System.Windows.Forms.Button markWarOk;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox2;
    }
}
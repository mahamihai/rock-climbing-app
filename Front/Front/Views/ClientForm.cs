﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Front.Services;
using System.IO;
using Front.Models.API;
using Front.Models;

namespace Front
{
    public partial class ClientForm : Form
    {
        private ClientService _client;
        public ClientForm(ClientService _client)
        {
            InitializeComponent();
            
            this._client = _client;
            ShowMountains();
        }

        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

 
        private int selctedMountainId, selectedArealId, selectedRouteId, selectedWarningId;

        private Byte[] image;
        private string warningDescription;

        //Mountain Range........................................................................

        private void MountainsGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int id;

            try
            {
                int row = e.RowIndex;
                string value = this.MountainsGridView.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.selctedMountainId = id;
        }

        private void ShowMountains()
        {
            var mountains = this._client.GetMountains();
            this.MountainsGridView.DataSource = mountains;
        }
        /*
           var areals = this._client.GetArealsById(this.selctedMountainId);
           this.ArealGridView.DataSource = areals;
        */
        
        


        //Areal....................................


        private void ArealGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id;

            try
            {
                int row = e.RowIndex;
                string value = this.ArealGridView.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.selectedArealId = id;
        }

        private void TrailGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id;

            try
            {
                int row = e.RowIndex;
                string value = this.TrailGridView.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.selectedRouteId = id;
        }

        //Trail.................................................................................
        private void TrailGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int id;

            try
            {
                int row = e.RowIndex;
                string value = this.TrailGridView.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.selectedRouteId = id;
        }
        private Image getImage(byte[] src)
        {
            MemoryStream ms = new MemoryStream(src);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
        private void SelectRoute_Click(object sender, EventArgs e)
        {
            var route = this._client.GetRouteById(this.selectedRouteId);
            var warnings = this._client.getWarningByRouteId(this.selectedRouteId);
            this.dataGridView1.DataSource = warnings;
            if(route.Topo!=null)
            {
                Image topoImage = this.getImage(route.Topo);
                this.pictureBox1.Image = topoImage;
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            }

           

        }


        //Create Route...............................................................

        private void UploadImage_Click(object sender, EventArgs e)
        {
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            image = ReadFully(myStream);

                        }
                    }
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        

   
        private void MountainsGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id;

            try
            {
                int row = e.RowIndex;
                string value = this.MountainsGridView.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.selctedMountainId = id;
        
        }

        private void ShowAreals_Click(object sender, EventArgs e)
        {
            var routes = this._client.GetArealsById(this.selctedMountainId);
            this.ArealGridView.DataSource = routes;
        }
        private void ShowRoutes_Click(object sender, EventArgs e)
        {
            var routes = this._client.GetRoutesByArealId(this.selectedArealId);
            this.TrailGridView.DataSource = routes;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {


            int id;

            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView1.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.selectedWarningId = id;


        }









        //Warnings.................................................................


        private void CreateWarning_Click(object sender, EventArgs e)
        {
            string warningDescription = this.textBox1.Text;
            string dangerLever = this.textBox2.Text;
            WarningAPI newWarning = new WarningAPI
            {
                Description = warningDescription,
                RouteId = this.selectedRouteId,
                MarkedAsResolved = false,
                DangerLever = dangerLever

            };

            this._client.createWarning(newWarning);

        }

        private void markWarOk_Click(object sender, EventArgs e)
        {
            this._client.MarkWarningOK(this.selectedWarningId);
        }
    }


    
}

    

